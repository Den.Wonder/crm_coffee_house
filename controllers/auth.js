const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const keys = require('../config/keys')
const User = require('../models/User')
const errorHandler = require('../utils/errorHandler')

module.exports.login = async function (req, res) {
    const candidate = await User.findOne({
        email: req.body.email
    })
    if(candidate){
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password)
        if(passwordResult){
            console.log('пароли совпали.')
        //    generation of token
            const token = jwt.sign(
                {
                email: candidate.email,
                userId: candidate._id
                },
                keys.jwt,
                {
                    expiresIn: 60 * 60
                }
            )
            res.status(200).json({
                token:`Bearer ${token}`
            })
        } else {
            console.log('пароли не совпадают!')
            res.status(401).json({
                message:'Пароль введён неверно. '
            })
        }
    } else{
        res.status(404).json({
            message: `Пользователя ${req.body.email} не существует!`
        })
    }
}

module.exports.register = async function(req, res){
   const candidate = await User.findOne({
       email: req.body.email
   })

    if(candidate){
        console.log(`Error! user ${req.body.email} is already registered`)
        res.status(409).json({
            message: 'Такой  email уже зарегистрирован!'
        })
    } else {
        const salt = bcrypt.genSaltSync(10)
        const password = req.body.password
        console.log('User created!')
        const user = new User({
            email: req.body.email,
            password: bcrypt.hashSync(password, salt)
        })

        try {
            //асинхронный код
            await user.save()
            res.status(201).json(user)
        } catch(e) {
        //    обработать ошибку
            errorHandler(res, e)
        }

    }
}
