const express = require('express');
const mongoose = require('mongoose')
const passport = require('passport')
const bodyParser = require('body-parser')
const analyticsRoutes = require('./routes/analytics')
const authRoutes = require('./routes/auth')
const categoryRoutes = require('./routes/category')
const orderRoutes = require('./routes/order')
const positionRoutes = require('./routes/position')
const keys = require('./config/keys')

//creating of express application
const app = express()

//connect to mongodb
mongoose.connect(keys.mongoURI)
    .then(()=> console.log('MongoDB connected!'))
    .catch(error=>console.log(error))

//for using passport
app.use(passport.initialize())
require('./middleware/passport')(passport)


//for parsing users data
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

//обработка запросов с других доменов
app.use(require('cors')())

//пакет для лучшего логирования запросов
app.use(require('morgan')('dev'))

//for access to upload images
app.use('/uploads', express.static('uploads'))

//routes
app.use('/api/analytics', analyticsRoutes)
app.use('/api/auth', authRoutes)
app.use('/api/category', categoryRoutes)
app.use('/api/order', orderRoutes)
app.use('/api/position', positionRoutes)

//export of module
module.exports = app
