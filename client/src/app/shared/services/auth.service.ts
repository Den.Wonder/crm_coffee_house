import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from '../interfaces'
import {tap} from "rxjs/operators";

@Injectable({
  //автоматическая регистрация сервиса в модуле (введено в ангуляре 6)
  providedIn: 'root'
})
export class AuthService{

  private token = null

  //Обращение к http запросу для асинхронного запроса на сервер
  //инжектируем http клиент для конструктора
  constructor(private http: HttpClient){

  }

  register(user: User):Observable<User>{
    return this.http.post<User>('/api/auth/register', user)
  }
  login(user: User): Observable<{token: string}> {
    //В бекенде в auth.js методы post, поэтому здесь тоже метод post
    return this.http.post<{token: string}>('/api/auth/login', user)
      .pipe(
        tap(
          ({token})=>{
            localStorage.setItem('auth-token',token)
            this.setToken(token)
          }
        )
      )
  }
  setToken(token: string){
    this.token = token
  }

  getToken(): string{
    return this.token
  }

  isAuthenticated():boolean{
    return !!this.token
  }

  logout(){
    this.setToken(null)
    localStorage.clear()
  }
}
