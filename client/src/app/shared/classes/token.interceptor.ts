import {Injectable} from "@angular/core";
import {AuthService} from "../services/auth.service";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {Router} from "@angular/router";

@Injectable()

export class TokenInterceptor implements HttpInterceptor{
  constructor(private auth:AuthService,
              private router:Router){

  }
//Перехватываем запрос
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//А здесь мы можем делать с этим запросом всё, что угодно
    //Наша задача - добавить хедеры авторизации к каждому запросу, как значение указываем токен.
    //Таким образом юзер будет обновлять токен на серваке при любом запросе, а значит счетчик времени авторизации будет обновляться
    //Говоря проще - его не выбросит из системы, если после последнего действия прошло менее часа
    if(this.auth.isAuthenticated()){
      req = req.clone({
        setHeaders: {
          Authorization: this.auth.getToken()
        }
      })
    }

    //Возвращаем запрос после перехвачивания
    return next.handle(req).pipe(
      catchError(
        (error: HttpErrorResponse) => this.handleAuthError(error)
      )
    )
  }
  private handleAuthError(error: HttpErrorResponse):Observable<any>{
      if(error.status == 401){
          this.router.navigate(['/login'],{
            queryParams:{
              sessionFailed: true
            }
          })
      }
      return throwError(error)
  }
}
