const mongoose = require('mongoose')

//Создаем схему с помощью конструктора mongoose.
const Schema = mongoose.Schema

//Описываем схему - какое поле, какой тип и т.п.
const categorySchema = new Schema({
    name:{
        type: String,
        required : true
    },
    imageSrc:{
        type: String,
        default: ''
    },
    user:{
        ref: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('categories', categorySchema)
